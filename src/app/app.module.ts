import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";

import { GoogleMaps } from "@ionic-native/google-maps";
import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Globalization } from "@ionic-native/globalization/ngx";
//servicios
import { UsuariosService } from "./servicios/usuarios.service";
//Componentes
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";

//Componentes
import { HeaderComponent } from "./asset/header/header.component";
import { NotificationsComponent } from "./asset/notifications/notifications.component";
import { SidebarComponent } from "./asset/sidebar/sidebar.component";
import { SidebarChatComponent } from "./asset/sidebar-chat/sidebar-chat.component";

//Traduciones
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

//selección de api RESTFUL
import { HttpClient, HttpClientModule } from "@angular/common/http";

// Modulo SocketIO
import { SocketIoModule, SocketIoConfig } from "ngx-socket-io";
const config: SocketIoConfig = { url: "http://localhost:3000", options: {} };

// Guard de verificación de token
import { AuthGuard } from "./Guard/auth.guard";
import { Select2Module } from "ng2-select2";

@NgModule({
	declarations: [
		AppComponent,
		HeaderComponent,
		NotificationsComponent,
		SidebarComponent,
		SidebarChatComponent,
	],
	entryComponents: [],
	imports: [
		BrowserModule,
		IonicModule.forRoot(),
		AppRoutingModule,
		HttpClientModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: (http: HttpClient) => {
					return new TranslateHttpLoader(http);
				},
				deps: [HttpClient],
			},
		}),
		SocketIoModule.forRoot(config),
		Select2Module,
	],
	providers: [
		StatusBar,
		SplashScreen,
		UsuariosService,
		AuthGuard,
		GoogleMaps,
		HeaderComponent,
		Globalization,
		{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
