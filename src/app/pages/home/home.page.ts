import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router'
import { Chart } from 'chart.js';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  
  linegraph:any;
  GraficaDeBarras:any;
  @ViewChild('lineCanvas') lineCanvas;
  @ViewChild('bargraph') bargraph;

  constructor(
  	private __router: Router
  	) { }

  ngOnInit() {
 
  }

  buscadorDoctores(){
  		this.__router.navigate(['maps']);
  }

  public obtenerlinegraph(){

  	this.linegraph = new Chart(this.lineCanvas.nativeElement,{

  		type: 'line',
  		data: {
			labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			datasets: [{
				label: "My First dataset",
				backgroundColor: "rgba(0, 158, 251, 0.5)",
				data: [100, 70, 20, 100, 120, 50, 70, 50, 50, 100, 50, 90]
				}, {
					label: "My Second dataset",
					backgroundColor: "rgba(255, 188, 53, 0.5)",
					fill: true,
					data: [28, 48, 40, 19, 86, 27, 20, 90, 50, 20, 90, 20]
				}]
			},
		options: {
			responsive: true,
			legend: {
				display: false,
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			}
		}
  	})

  }
  public obtenerBarGraph(){

  	this.GraficaDeBarras = new Chart(this.bargraph.nativeElement,{

  		type: 'line',
  		data: {
			labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
			datasets: [{
				label: 'Dataset 1',
				backgroundColor: 'rgba(0, 158, 251, 0.5)',
				borderColor: 'rgba(0, 158, 251, 1)',
				borderWidth: 1,
				data: [35, 59, 80, 81, 56, 55, 40]
			}, {
				label: 'Dataset 2',
				backgroundColor: 'rgba(255, 188, 53, 0.5)',
				borderColor: 'rgba(255, 188, 53, 1)',
				borderWidth: 1,
				data: [28, 48, 40, 19, 86, 27, 90]
			}]
		},
		options: {
			responsive: true,
			legend: {
				display: false,
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			}
		}
  	})

  }

}
