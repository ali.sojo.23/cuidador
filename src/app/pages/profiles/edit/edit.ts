import { Component, OnInit } from "@angular/core";
import { UsuariosService } from "../../../servicios/usuarios.service";
import { ActivatedRoute, Router } from "@angular/router";
import { LoadingController } from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";
import { environment } from "../../../../environments/environment";
import { AddressService } from "../../../servicios/address.service";
import { Observable } from "rxjs/Observable";

@Component({
	selector: "app-editar-mi-perfil",
	templateUrl: "./edit.html",
	styleUrls: ["./edit.scss"],
})
export class EditarMiPerfilPage implements OnInit {
	constructor(
		private __usuario: UsuariosService,
		private __route: ActivatedRoute,
		private __direccion: AddressService,
		private __ruta: Router,
		private __loader: LoadingController,
		private traductor: TranslateService
	) {
		this.obtenerUsuario();
		this.obtenerPais();
		this.urlSlug();
	}
	url: string = environment.urlHost;
	avatar: any;
	usuario: any;
	identificador: string;

	informacion: any = {
		birthday: "",
		gender: "",
		country: "",
		state: "",
		postalCode: "",
		phone: "",
	};
	country: any;
	ciudad: any;
	error: any;
	load: any;

	obtenerUsuario() {
		this.urlSlug();

		this.__usuario.mostrarUsuario(this.identificador).subscribe(
			(resultado) => {
				this.usuario = resultado.user;
				if (this.usuario.informacion) {
					this.informacion = {
						birthday: this.usuario.informacion.Birthdate,
						gender: this.usuario.informacion.Gender,
						country: this.usuario.informacion.Country,
						state: this.usuario.informacion.Province,
						address: this.usuario.informacion.Address,
						phone: this.usuario.informacion.Phone,
					};
				}
			},
			(error) => {
				console.error(error);
			}
		);
	}

	urlSlug() {
		this.identificador = this.__route.snapshot.paramMap.get("id");
	}

	obtenerPais() {
		this.__direccion.obtenerPaises().subscribe(
			(resultado) => {
				this.country = resultado.country;
			},
			(error) => {
				console.error(JSON.stringify(error));
			}
		);
	}
	obtenerCiudades(identificador, usuario) {
		if (usuario.informacion === null) {
			this.informacion.state = "";
		}

		this.__direccion.obtenerCiudades(identificador).subscribe(
			(resultado) => {
				this.ciudad = resultado.province;
			},
			(error) => {
				console.error(JSON.stringify(error));
			}
		);
	}

	almacenarInformacion() {
		let save: any = {
			usuario: this.usuario,
			informacion: this.informacion,
		};

		this.__usuario.actualizarUsuarios(save, this.usuario._id).subscribe(
			async (resultado) => {
				this.obtenerUsuario();

				let info = localStorage.getItem("inf_tk");
				if (info == "undefined") {
					localStorage.setItem("inf_tk", resultado.user._id);
				}
				await this.__ruta.navigate(["/perfil", this.usuario.Auth._id]);
				window.location.reload();
			},
			(error) => {
				console.error(error);
			}
		);
	}
	async cargarImagen(event, usuario) {
		this.load = await this.__loader.create({
			message: "Cargando imagen al servidor. Por favor espere",
		});

		this.load.present();
		let reader: any = new FileReader();
		if (event.target.files && event.target.files.length > 0) {
			let file = event.target.files[0];
			console.log(file);
			if (file.type != "image/jpeg" && file.type != "image/png") {
				console.log("error 1");

				this.load.onDidDismiss();
				this.error =
					"Verifique su tipo de archivo solo puede cargar archivos en formato PNG o JPEG,JPG";
				return true;
			}
			if (file.size > 1000000) {
				this.load.onDidDismiss();
				this.error =
					"Error al cargar la imaágen por favor seleccione una imagen menor a 1Mb";
				return false;
			}

			reader.readAsDataURL(file);
			reader.onload = () => {
				this.usuario.avatar = reader.result.split(",")[1];
				console.log(this.usuario.avatar);

				this.__usuario
					.modificarAvatar(this.usuario.avatar, usuario)
					.subscribe(
						(resultado) => {
							this.load.dismiss();
							console.log(resultado);
							this.obtenerUsuario();
						},
						(error) => {
							console.error(error);

							this.load.dismiss();
							this.obtenerUsuario();
						}
					);
			};
		}
	}

	setLang() {
		this.traductor.setDefaultLang("en");
		let lang = localStorage.getItem("prefer_lang");

		this.traductor.use(lang);
	}

	ngOnInit() {
		this.setLang();
	}
}
