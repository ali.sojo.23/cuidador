import { Component, OnInit } from "@angular/core";
import { Socket } from "ngx-socket-io";

@Component({
	selector: "app-chat",
	templateUrl: "./chat.page.html",
	styleUrls: ["./chat.page.scss"],
})
export class ChatPage implements OnInit {
	message = "";
	messages = [];
	currentUser = "";

	constructor(private socket: Socket) {}

	ngOnInit() {
		// this.socket.connect();

		let name = `user-${new Date().getTime()}`;
		this.currentUser = name;

		this.socket.emit("set-name", name);

		this.socket.fromEvent("users-changed").subscribe((data) => {
			let user = data["user"];
			if (data["event"] === "left") {
				console.log("User left: " + user);
			} else {
				console.log("User joined: " + user);
			}
		});

		this.socket.fromEvent("message").subscribe((message) => {
			this.messages.push(message);
		});
	}

	sendMessage() {
		this.socket.emit("send-message", { text: this.message });
		this.message = "";
	}

	ionViewWillLeave() {
		this.socket.disconnect();
	}
}
