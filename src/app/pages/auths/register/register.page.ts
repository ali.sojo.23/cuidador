import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../../servicios/auth.service";
import { Router } from "@angular/router";
import { LoadingController } from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-register",
	templateUrl: "./register.page.html",
	styleUrls: ["./register.page.scss"],
})
export class RegisterPage implements OnInit {
	constructor(
		private __auth: AuthService,
		private router: Router,
		private loader: LoadingController,
		private traductor: TranslateService
	) {
		this.setLang();
	}

	Registro: any = {
		FirstName: "",
		LastName: "",
		email: "",
		password: "",
		confirm_password: "",
		Role: 100,
	};
	Error: string;
	async registrarUsuario() {
		let load = await this.loader.create({ message: "por favor espere" });
		load.present();

		this.__auth.registroUsuario(this.Registro).subscribe(
			(resultado) => {
				const user = resultado.User;
				this.__auth.localStorage(user);
				load.dismiss();
				this.router.navigate(["/perfil/editar", user.user.Auth]);
			},
			(error) => {
				load.dismiss();
				console.log(error);
				this.Error = error;
			}
		);
	}

	setLang() {
		let lang = localStorage.getItem("prefer_lang");

		this.traductor.use(lang).subscribe(
			(next) => null,
			(error) => {
				console.log(error);
			}
		);
	}

	ngOnInit() {}
}
