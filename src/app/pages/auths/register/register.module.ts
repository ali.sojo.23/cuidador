import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { IonicModule } from "@ionic/angular";

import { RegisterPage } from "./register.page";
import { HttpClient } from "@angular/common/http";

const routes: Routes = [
	{
		path: "",
		component: RegisterPage,
	},
];

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
		RouterModule.forChild(routes),
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: (http: HttpClient) => {
					return new TranslateHttpLoader(http);
				},
				deps: [HttpClient],
			},
		}),
	],
	declarations: [RegisterPage],
})
export class RegisterPageModule {}
