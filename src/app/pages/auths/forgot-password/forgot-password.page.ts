import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../../servicios/auth.service";
import { AlertController } from "@ionic/angular";
import { from } from "rxjs";
import { text } from "@angular/core/src/render3";
@Component({
	selector: "app-forgot-password",
	templateUrl: "./forgot-password.page.html",
	styleUrls: ["./forgot-password.page.scss"],
})
export class ForgotPasswordPage implements OnInit {
	constructor(
		private __auth: AuthService,
		private __Alert: AlertController
	) {}
	mail: any = {
		email: "",
	};
	Error: any;
	response: any;
	async recuperarPassword() {
		let Alert = await this.__Alert.create({
			message:
				"Se ha enviado un correo electrónico de recuperación de contraseña, verifique su buzón de entrada y siga los pasos que ahí se le indica",
			buttons: [
				{
					text: "Ok",
					role: "Submit",
				},
			],
		});
		Alert.present();
		let Error = await this.__Alert.create({
			message: "El usuario ingresado no fue encontrado",
			buttons: [
				{
					text: "OK",
					role: "Cancel",
				},
			],
		});
		this.__auth.resetPassword(this.mail).subscribe(
			(respuesta) => {
				if (respuesta.code == 404) {
					Alert.dismiss();
					Error.present();
					this.Error = 404;
				} else {
					this.response = respuesta;
					console.log(respuesta);
					document.location.href = "/auth/login";
				}
			},
			(error) => {
				console.log(error);
			}
		);
	}

	ngOnInit() {}
}
