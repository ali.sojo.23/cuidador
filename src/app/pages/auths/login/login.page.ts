import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../../servicios/auth.service";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { environment } from "src/environments/environment";

@Component({
	selector: "app-login",
	templateUrl: "./login.page.html",
	styleUrls: ["./login.page.scss"],
})
export class LoginPage implements OnInit {
	Login: any = {
		email: "",
		password: "",
	};
	Error: string;
	constructor(
		private __auth: AuthService,
		private router: Router,
		private traductor: TranslateService
	) {
		//Prefer Lang
		this.traductor.setDefaultLang("en");
	}
	//Funcion para iniciar sesión en el sisteme
	iniciarSesion() {
		this.__auth.iniciarSesion(this.Login).subscribe(
			async (resultado) => {
				const user = resultado.User;

				if (user.user.Role != 100) {
					return (this.Error = "No Authorized");
				} else {
					let id = user.user.Auth;
					//Almacenar informacion de los token en el local storage
					this.__auth.localStorage(user);
					//Si existe la información del usuario ir al storage
					//en caso contrario ir a editar usuario

					if (user.user.informacion.Country) {
						await this.router.navigate(["dashboard"]);
						window.location.reload();
					} else {
						await this.router.navigate(["perfil/editar/" + id]);
						window.location.reload();
					}
				}
			},
			(error) => {
				this.Error = error.error;
			}
		);
	}

	setLang() {
		let lang = localStorage.getItem("prefer_lang");

		this.traductor.use(lang);
	}

	ngOnInit() {
		this.setLang();
	}
}
