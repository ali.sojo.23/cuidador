import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifyChatsPage } from './notify-chats.page';

describe('NotifyChatsPage', () => {
  let component: NotifyChatsPage;
  let fixture: ComponentFixture<NotifyChatsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotifyChatsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotifyChatsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
