import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

@Injectable({
	providedIn: "root",
})
export class AuthService {
	constructor(private httpClient: HttpClient) {}

	isLogged: boolean = false;
	url = environment.urlHost;
	urlRegistro: string = this.url + "auth/register";
	urlLogin: string = this.url + "auth/login";
	urlResetPassword: string = this.url + "auth/forgot/email";
	urlVerify: string = this.url + "auth/verify/resend/";

	iniciarSesion(data: any): Observable<any> {
		let Json = JSON.stringify(data);

		let headers = new HttpHeaders().set("Content-Type", "application/json");

		return this.httpClient.post(this.urlLogin, Json, { headers: headers });
	}
	registroUsuario(data: any): Observable<any> {
		let Json = JSON.stringify(data);

		let headers = new HttpHeaders().set("Content-Type", "application/json");

		return this.httpClient.post(this.urlRegistro, Json, {
			headers: headers,
		});
	}

	localStorage(data: any) {
		let token: string = data.accessToken;

		sessionStorage.setItem("tk_init", token);
		sessionStorage.setItem("exp_tk", data.expiresIn);
		sessionStorage.setItem("inf_tk", data.user.informacion._id);
		sessionStorage.setItem("import_data", data.user.Auth);
	}

	verifyToken() {
		let token: string = sessionStorage.getItem("tk_init");
		if (!token) {
			this.isLogged = false;
		} else {
			this.isLogged = true;
		}
	}

	deletelocalStorage() {
		sessionStorage.removeItem("tk_init");
		sessionStorage.removeItem("import_data");
	}

	resetPassword(data: any): Observable<any> {
		let Json = JSON.stringify(data);
		let headers = new HttpHeaders().set("Content-Type", "application/json");
		sessionStorage.setItem("rst_psw_mail", data);
		return this.httpClient.post(this.urlResetPassword, Json, {
			headers: headers,
		});
	}

	verifyEmail(id): Observable<any> {
		let headers = new HttpHeaders().set("Content-Type", "application/json");
		return this.httpClient.post(this.urlVerify + id, { headers: headers });
	}
}
