import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { environment } from "../../environments/environment";

@Injectable({
	providedIn: "root",
})
export class ExperienceService {
	constructor(private http: HttpClient) {}

	url = environment.urlHost;
	urlExperiences: string = this.url + "experiences/";
	header: any;
	setHeaders() {
		let token = sessionStorage.getItem("tk_init");
		this.header = new HttpHeaders()
			.set("Accept", "application/json")
			.set("access-token", token);
	}
	create(data, identificador): Observable<any> {
		let json = {
			data: data,
			id: identificador,
		};
		this.setHeaders();
		return this.http.post(this.urlExperiences, json, {
			headers: this.header,
		});
	}
	get(identificador): Observable<any> {
		this.setHeaders();
		return this.http.get(this.urlExperiences + identificador, {
			headers: this.header,
		});
	}
}
