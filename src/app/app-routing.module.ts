import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./Guard/auth.guard";
import { LogedGuard } from "./Guard/loged.guard";

const routes: Routes = [
	{ path: "", redirectTo: "dashboard", pathMatch: "full" },
	// Auth
	{
		path: "auth",
		children: [
			{
				path: "login",
				loadChildren:
					"./pages/auths/login/login.module#LoginPageModule",
			},
			{
				path: "forgot-password",
				loadChildren:
					"./pages/auths/forgot-password/forgot-password.module#ForgotPasswordPageModule",
			},
			{
				path: "register",
				loadChildren:
					"./pages/auths/register/register.module#RegisterPageModule",
			},
		],
	},
	//Dashboard
	{
		path: "dashboard",
		loadChildren: "./pages/home/home.module#HomePageModule",
		canActivate: [AuthGuard],
	},
	//Notifications
	{
		path: "notifications",
		loadChildren:
			"./pages/notifications/notifications.module#NotificationsPageModule",
		canActivate: [AuthGuard],
	},
	//Perfil
	{
		path: "perfil",
		canActivate: [AuthGuard],
		children: [
			{
				path: ":id",
				children: [
					{
						path: "",
						loadChildren:
							"./pages/profiles/show/perfil.module#PerfilPageModule",
					},
					{
						path: "estudios",
						loadChildren:
							"./pages/profiles/asset/estudios/estudios.module#EstudiosPageModule",
					},
					{
						path: "trabajos",
						loadChildren:
							"./pages/profiles/asset/trabajos/trabajos.module#TrabajosPageModule",
						canActivate: [AuthGuard],
					},
				],
			},
			{
				path: "editar/:id",
				loadChildren:
					"./pages/profiles/edit/edit.module#EditarMiPerfilPageModule",
			},
		],
	},
	//Notify Chats
	{
		path: "notify-chats",
		loadChildren:
			"./pages/notify-chats/notify-chats.module#NotifyChatsPageModule",
		canActivate: [AuthGuard],
	},
	//Maps
	{
		path: "maps",
		loadChildren: "./pages/maps/maps.module#MapsPageModule",
		canActivate: [AuthGuard],
	},
	//Mobile-menu
	{
		path: "mobile-menu",
		loadChildren:
			"./pages/mobile-menu/mobile-menu.module#MobileMenuPageModule",
	},
	//Chats
	{
		path: "chat",
		loadChildren: "./pages/chat/chat.module#ChatPageModule",
		canActivate: [AuthGuard],
	},
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
	],
	exports: [RouterModule],
})
export class AppRoutingModule {}
