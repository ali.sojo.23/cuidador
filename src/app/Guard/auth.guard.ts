import { Injectable } from "@angular/core";
import {
	ActivatedRouteSnapshot,
	RouterStateSnapshot,
	UrlTree,
	Router,
	CanActivate,
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../servicios/auth.service";

@Injectable({
	providedIn: "root",
})
export class AuthGuard implements CanActivate {
	constructor(private authService: AuthService, private router: Router) {}

	canActivate() {
		this.authService.verifyToken();

		if (!this.authService.isLogged) {
			console.log("El usuario no está loggeado");
			this.router.navigate(["/auth/login"]);
			return false;
		}

		return true;
	}
}
